<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$object = new Animal("shaun");

echo "Nama hewan : " . $object->name . "<br>";
echo "Jumlah kaki : " . $object->leg . "<br>";
echo "Darah dingin : " . $object->cold_blooded . "<br><br>";

$object2 = new Ape("kera sakti");

echo "Nama hewan : " . $object2->name . "<br>";
echo "Jumlah kaki : " . $object2->leg . "<br>";
echo "Darah dingin : " . $object2->cold_blooded . "<br>";
$object2->yell() . "<br><br>";

$object3 = new Frog("buduk");

echo "Nama hewan : " . $object3->name . "<br>";
echo "Jumlah kaki : " . $object3->leg . "<br>";
echo "Darah dingin : " . $object3->cold_blooded . "<br>";
$object3->jump();
