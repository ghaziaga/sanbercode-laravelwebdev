@extends('layout.master')
@section('title')
Halaman Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3"> Tambah Caster </a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Caster</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th>{{$key+1}}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td> 
                    <form action="/cast/{{$item->id}}" method="post">
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
                
            </tr>
            
        @empty
        <tr>
            <td>Data Caster Masih Kosong</td>
        </tr>
            
        @endforelse
    </tbody>
</table>


@endsection