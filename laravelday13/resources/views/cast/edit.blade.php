@extends('layout.master')
@section('title')
Halaman Edit Caster
@endsection

@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')

    <div class="form-group">
        <label>Nama Caster</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur Caster</label>
        <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio Caster</label>
        {{-- <textarea name="bio" class="form-control" cols="30" rows="10" value="{{$cast->bio}}"></textarea> --}}
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}">
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection