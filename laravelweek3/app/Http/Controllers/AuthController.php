<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        return view('welcome');
    }

    public function welcome_post(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

   
        // return "$firstname" . "  " . "$lastname";
        return view('welcome',['firstname'=>$firstname,'lastname'=>$lastname]);
    }
}
